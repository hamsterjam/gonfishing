using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RodManager : MonoBehaviour {
    [SerializeField] private IRodBase[] _rods = {};
    [SerializeField] private ClickDetector _detector = null;
    [SerializeField] private FishManager  _fishManager  = null;
    [SerializeField] private CountManager _countManager = null;
    [SerializeField] private CameraTrack  _cameraTrack  = null;

    [SerializeField] private GameObject _winDisplay = null;

    private int _currentRod = 0;

    public bool hasAttachedFish {
        get { return _rods[_currentRod].attachedFish != null; }
    }

    private void Start() {
        _detector.pointerDownAction += () => {
            var rod = _rods[_currentRod];
            rod.pull = true;

            if (rod.finished && rod.attachedFish != null) {

                if (rod.attachedFish.type == 4) {
                    ShowEnd();
                    return;
                }

                var fish = rod.attachedFish;
                rod.attachedFish = null;

                    // Master
                _countManager.AddCount(fish.type, 1);
                _fishManager.RemoveFish(fish);
            }
            else if (rod.finished) {
                rod.Cast(10f);
            }
        };

        _detector.pointerUpAction += () => {
            var rod = _rods[_currentRod];
            rod.pull = false;
        };
    }

    private void ShowEnd() {
        _winDisplay.SetActive(true);
    }

    public void SetCurrentRod(int rod) {
        Escape();
        _rods[_currentRod].gameObject.SetActive(false);
        _rods[rod].gameObject.SetActive(true);

        _cameraTrack.trackee = _rods[rod].Hook.transform;
        _fishManager.UpdateHook(_rods[rod].Hook.transform);
        _currentRod = rod;
    }

    public void Escape() {
        var rod = _rods[_currentRod];
        if (rod.attachedFish == null) return;

        Destroy(rod.attachedFish.gameObject.GetComponent<LockPosition>());
        rod.attachedFish = null;
    }
}
