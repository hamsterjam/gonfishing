﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTrack : MonoBehaviour {

    public Transform trackee = null;
    [SerializeField] private Vector2 _clampMin = Vector2.zero;
    [SerializeField] private Vector2 _clampMax = Vector2.zero;

    private void Update() {
        Vector2 newPos = trackee.position;

        newPos.x = Mathf.Clamp(newPos.x, _clampMin.x, _clampMax.x);
        newPos.y = Mathf.Clamp(newPos.y, _clampMin.y, _clampMax.y);

        transform.position = newPos;
    }
}
