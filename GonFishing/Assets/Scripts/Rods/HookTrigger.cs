using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class HookTrigger : MonoBehaviour {

    [SerializeField] private IRodBase _rod = null;

    private Collider2D _collider;

    private void Start() {
        _collider = GetComponent<Collider2D>();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (_rod.attachedFish != null) return;

        var lockPos = other.attachedRigidbody.gameObject.AddComponent<LockPosition>();
        lockPos.connectedBody = transform;
        _rod.attachedFish = other.attachedRigidbody.GetComponent<FishController>();
    }
}
