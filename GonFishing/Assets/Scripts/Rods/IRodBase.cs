using System.Collections;
using UnityEngine;

public abstract class IRodBase : MonoBehaviour {
    protected static readonly float minDistance = 0.8f;
    protected static readonly float pullSpeed   = 2f;

    [SerializeField] protected Rigidbody2D  _hook    = null;
    [SerializeField] protected GaugeManager _gauge   = null;
    [SerializeField] protected float _bothPullRate   = 0.8f;
    [SerializeField] protected float _singlePullRate = 0.4f;

    public Rigidbody2D Hook { get { return _hook; } }

    public bool pull = false;
    public bool finished = false;
    public FishController attachedFish = null;
    protected bool fishPulling = false;
    protected bool pullRoutine = false;

    public abstract void Cast(float strength);

    protected IEnumerator FishPull(FishController fish) {
        pullRoutine = true;
        yield return new WaitForSeconds(Random.Range(fish.MinPullDelay, fish.MaxPullDelay));
        fishPulling = true;
        yield return new WaitForSeconds(Random.Range(fish.MinPullDuration, fish.MaxPullDuration));
        pullRoutine = false;
        fishPulling = false;
    }

}
