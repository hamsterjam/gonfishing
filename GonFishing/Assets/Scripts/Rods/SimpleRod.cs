using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleRod : IRodBase {
    [SerializeField] private Transform   _castPoint = null;

    [SerializeField] private DistanceJoint2D _line  = null;
    [SerializeField] private float _lineLength = 10f;

    private Vector2 _dir = (new Vector2(-1, 1)).normalized;

    public void OnEnable() {
        Cast(10f);
    }

    public override void Cast(float strength) {
        finished = false;
        _line.distance = _lineLength;
        _hook.transform.position = _castPoint.position;
        _hook.velocity = _dir * strength;
    }

    public void Update() {
        if (pull && !finished) {
            _line.distance -= pullSpeed * Time.deltaTime;
            if (_line.distance < minDistance) {
                finished = true;
            }
        }

        if (attachedFish != null) {
            if (!pullRoutine) StartCoroutine(FishPull(attachedFish));

            if (fishPulling) {
                _hook.velocity = attachedFish.PullVector;
            }

            if (pull && fishPulling) {
                // Both pulling and fish pulling
                _gauge.AddValue(_bothPullRate * Time.deltaTime);
            }
            else if (pull || fishPulling) {
                // Only one pulling
                _gauge.AddValue(_singlePullRate * Time.deltaTime);
            }
        }
        else if (pull) {
            // Fish can't be pulling, so only one pulling
            _gauge.AddValue(_singlePullRate * Time.deltaTime);
        }
    }
}
