using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectionController : MonoBehaviour {
    [System.Serializable]
    private class Info {
        public Button button = null;
        public GameObject disableLayer = null;
        public int[] reqs = {};
    }

    [SerializeField] private Button _openButton  = null;
    [SerializeField] private Button _closeButton = null;
    [SerializeField] private GameObject _window  = null;

    [SerializeField] private CountManager _countManager = null;
    [SerializeField] private RodManager _rodManager = null;

    [SerializeField] private Info[] _selections = {};

    private void Start() {
        _openButton.onClick.AddListener(() => {
            Time.timeScale = 0f;
            UpdateUnlocks();
            _window.SetActive(true);
        });

        _closeButton.onClick.AddListener(() => {
            Time.timeScale = 1f;
            _window.SetActive(false);
        });

        for (int i = 0; i < _selections.Length; ++i) {
            SetupButton(_selections[i].button, i);
        }
    }

    private void SetupButton(Button button, int rod) {
        button.onClick.AddListener(() => {
            _rodManager.SetCurrentRod(rod);
        });
    }

    private void UpdateUnlocks() {
        foreach (var info in _selections) {
            bool unlock = true;
            for (int type = 0; type < 4; ++type) {
                if (_countManager.GetCount(type) < info.reqs[type]) {
                    unlock = false;
                    break;
                }
            }
            info.disableLayer.SetActive(!unlock);
        }
    }
}
