﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LineBetweenObjects : MonoBehaviour {

    [SerializeField] private Transform _object1 = null;
    [SerializeField] private Transform _object2 = null;

    [SerializeField] private Vector3 _offset1 = Vector3.zero;
    [SerializeField] private Vector3 _offset2 = Vector3.zero;

    [SerializeField] private int _numPoints = 20;
    [SerializeField] private DistanceJoint2D _joint = null;

    private LineRenderer _renderer;

    private void Start() {
        _renderer = GetComponent<LineRenderer>();
    }

    private void Update() {
        LineArcCalculator calc = new LineArcCalculator(
            _object1.position + _offset1,
            _object2.position + _offset2
        );

        calc.SetArcLength(_joint.distance);

        _renderer.positionCount = _numPoints;
        _renderer.SetPositions(calc.GetPoints(_numPoints));
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        Start();
        Update();
    }
#endif
}
