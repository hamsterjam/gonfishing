﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BobberRod : IRodBase {
    private static readonly float bobberDelay = 0.2f;

    [SerializeField] private Rigidbody2D _bobber    = null;
    [SerializeField] private Transform   _castPoint = null;

    [SerializeField] private DistanceJoint2D _line1 = null;
    [SerializeField] private DistanceJoint2D _line2 = null;

    [SerializeField] private float _line1Length = 10f;
    [SerializeField] private float _line2Length = 4f;

    private Vector2 _dir = (new Vector2(-1, 1)).normalized;

    public void OnEnable() {
        Cast(10f);
    }

    public void Update() {
        if (pull && !finished) {
            DistanceJoint2D line = (_line1.distance < minDistance) ? _line2 : _line1;
            line.distance -= pullSpeed * Time.deltaTime;

            if (line == _line2 && line.distance < minDistance) {
                finished = true;
            }
        }

        if (attachedFish != null) {
            if (!pullRoutine) StartCoroutine(FishPull(attachedFish));

            if (fishPulling) {
                _hook.velocity = attachedFish.PullVector;
            }

            if (pull && fishPulling) {
                // Both pulling and fish pulling
                _gauge.AddValue(_bothPullRate * Time.deltaTime);
            }
            else if (pull || fishPulling) {
                // Only one pulling
                _gauge.AddValue(_singlePullRate * Time.deltaTime);
            }
        }
        else if (pull) {
            // Fish can't be pulling, so only one pulling
            _gauge.AddValue(_singlePullRate * Time.deltaTime);
        }
    }

    public override void Cast(float strength) {
        finished = false;

        _line1.distance = _line1Length;
        _line2.distance = _line2Length;

        _hook.transform.position   = _castPoint.position;
        _bobber.transform.position = _castPoint.position;

        _hook.velocity   = _dir * strength;
        _bobber.velocity = Vector2.zero;

        StartCoroutine(DelayCastBobber(strength, bobberDelay));
    }

    private IEnumerator DelayCastBobber(float strength, float delay) {
        yield return new WaitForSeconds(delay);
        _bobber.velocity = _dir * strength;
    }
}
