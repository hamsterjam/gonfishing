using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountManager : MonoBehaviour {

    private static readonly string countFormat = "x {0}";

    [SerializeField] private Text[] _displays = {};

    private int[] _count;

    public void Start() {
        _count = new int[_displays.Length];
        for (int i = 0; i < _displays.Length; ++i) SetCount(i, 0);
    }

    public void SetCount(int type, int count) {
        _count[type] = count;
        _displays[type].text = string.Format(countFormat, count);
    }

    public void AddCount(int type, int addition) {
        SetCount(type, _count[type] + addition);
    }

    public int GetCount(int type) {
        return _count[type];
    }
}
