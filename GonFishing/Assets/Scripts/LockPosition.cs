using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockPosition : MonoBehaviour {
    public Transform connectedBody;

    private void LateUpdate() {
        Vector3 newPos = connectedBody.position;
        newPos.z = transform.position.z;
        transform.position = newPos;
    }
}
