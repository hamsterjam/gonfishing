using System;
using UnityEngine;

public class LineArcCalculator {
    private static readonly int   lenCalcSegments = 10;
    private static readonly float tolerance = 0.001f;
    private static readonly int   maxItter  = 200;

    private float x1, y1, z1;
    private float x2, y2, z2;

    private float x0, y0;

    private float a;
    private float b;

    private bool paramsSet = false;

    public LineArcCalculator(Vector3 pos1, Vector3 pos2) {
        this.x1 = pos1.x;
        this.x2 = pos2.x;

        this.y1 = pos1.y;
        this.y2 = pos2.y;

        this.z1 = pos1.z;
        this.z2 = pos2.z;

        x0 = x2 - x1;
        y0 = y2 - y1;
    }

    public void TempSetA(float a) {
        paramsSet = true;

        this.a = a;
        this.b = CalcBFromA(a);
    }

    public void SetArcLength(float s)
    {
        float lo  = 0f;
        float hi  = 0.1f;

        float len = CalcArcLength(hi, lenCalcSegments);

        while (len < s) {
            hi *= 2f;
            len = CalcArcLength(hi, lenCalcSegments);
        }

        int itter = 0;
        float a = hi;
        while (Math.Abs(s - len) > tolerance) {
            if (itter > maxItter) break;

            a   = (hi + lo) / 2;
            len = CalcArcLength(a, lenCalcSegments);

            if (len < s) {
                lo = a;
            }
            else {
                hi = a;
            }

            ++itter;
        }

        this.a = a;
        this.b = CalcBFromA(a);
        paramsSet = true;
    }

    public Vector3[] GetPoints(int numPoints) {
        if (!paramsSet)    throw new InvalidOperationException("Parameters not set!");
        if (numPoints < 2) throw new InvalidOperationException("Requires at least 2 points!");

        float xDiff = x2 - x1;
        float zDiff = z2 - z1;

        Vector3[] ret = new Vector3[numPoints];

        for (int i = 0; i < numPoints; ++i) {
            if (i == 0) {
                ret[i] = new Vector3(x1, y1, z1);
                continue;
            }
            if (i == numPoints - 1) {
                ret[i] = new Vector3(x2, y2, z2);
                continue;
            }

            float x = x1 + xDiff * i / (numPoints - 1);
            float y = ((a * (x - x1)) + b ) * (x - x1) + y1;
            float z = z1 + zDiff * i / (numPoints - 1);

            ret[i] = new Vector3(x, y, z);
        }

        return ret;
    }

    private float CalcArcLength(float a, int numPoints) {
        float ret = 0f;
        Vector2 prev = Vector2.zero;

        this.a = a;
        this.b = CalcBFromA(a);

        for (int i = 1; i < numPoints; ++i) {
            Vector2 point;
            if (i == numPoints - 1) {
                point = new Vector2(x0, y0);
            }
            else {
                float x = x0 * i / (numPoints - 1);
                float y = ((a * x) + b) * x;
                point = new Vector2(x, y);
            }

            ret += (point - prev).magnitude;
            prev = point;
        }

        return ret;
    }

    private float CalcBFromA(float a) {
        return y0 / x0 - a * x0;
    }
}
