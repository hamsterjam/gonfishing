﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class FishController : MonoBehaviour {

    [SerializeField] private RodManager _rodManager = null;
    [SerializeField] private Transform _hook = null;
    public RodManager RodManager { set { _rodManager = value; } }
    public Transform Hook { set { _hook = value; } }

    [SerializeField] private Vector2 _wanderDirection = Vector2.one;

    [SerializeField] private float _normalSpeed   = 10f;
    [SerializeField] private float _sightedSpeed  = 20f;
    [SerializeField] private float _normalDelay   = 2f;
    [SerializeField] private float _sightedDelay  = 1f;

    [SerializeField] private float _sightDistance = 5f;
    [SerializeField] private float _sightAngle    = 20f;
    [SerializeField] private float _angleScale    = 50f;

    [SerializeField] private float _minPullDelay    = 1f;
    [SerializeField] private float _maxPullDelay    = 3f;
    [SerializeField] private float _minPullDuration = 0.5f;
    [SerializeField] private float _maxPullDuration = 1f;

    [SerializeField] private Vector2 _pullVector = Vector2.zero;

    public float MinPullDelay { get { return _minPullDelay; } }
    public float MaxPullDelay { get { return _maxPullDelay; } }
    public float MinPullDuration { get { return _minPullDuration; } }
    public float MaxPullDuration { get { return _maxPullDuration; } }

    public Vector2 PullVector { get { return _pullVector; } }

    public int type = 0;

    private Rigidbody2D _rigidbody;
    private SpriteRenderer _sprite;

    private bool _seesHook = true;

    private void Start() {
        _rigidbody = GetComponent<Rigidbody2D>();
        _sprite    = GetComponent<SpriteRenderer>();
        _wanderDirection.Normalize();

        StartCoroutine(PeriodicMove());
    }

    private IEnumerator PeriodicMove() {
        for (;;) {
            yield return new WaitForSeconds(_seesHook ? _sightedDelay : _normalDelay);

            Vector2 hookVec = _hook.position - transform.position;
            Vector2 dirVec  = _sprite.flipX ? Vector2.right : Vector2.left;

            float hookAngle     = 2 * Vector2.Angle(hookVec, dirVec);
            float hookDistance  = hookVec.magnitude;
            float adjSightAngle = _sightAngle + _angleScale / hookDistance - _angleScale / _sightDistance;

            _seesHook = (hookDistance <= _sightDistance && hookAngle <= adjSightAngle);
            if (_rodManager.hasAttachedFish) _seesHook = false;

            Vector2 vel;
            if (_seesHook) {
                vel  = hookVec.normalized;
                vel *= _sightedSpeed;
            }
            else {
                vel    = _wanderDirection * _normalSpeed;
                vel.x *= Random.Range(0, 2) * 2 - 1;
                vel.y *= Random.Range(0, 2) * 2 - 1;
            }

            _sprite.flipX = (vel.x > 0);
            _rigidbody.velocity = vel;
        }
    }
}
