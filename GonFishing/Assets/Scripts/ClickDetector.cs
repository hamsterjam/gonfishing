using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickDetector : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
    public Action pointerDownAction = null;
    public Action pointerUpAction   = null;

    bool isDown = false;

    public void OnPointerDown(PointerEventData e) {
        isDown = true;
        pointerDownAction?.Invoke();
    }

    public void OnPointerUp(PointerEventData e) {
        isDown = false;
        pointerUpAction?.Invoke();
    }

    private void OnApplicationFocus(bool hasFocus) {
        if (!hasFocus && isDown) {
            isDown = false;
            pointerUpAction?.Invoke();
        }
    }
}
