using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishManager : MonoBehaviour {
    [System.Serializable]
    private class Info {
        public GameObject prefab = null;

        public int   maxCount = 10;
        public float minDepth = 0f;
        public float maxDepth = 5f;
    }

    [SerializeField] private RodManager _rodManager = null;
    [SerializeField] private float _spawnDelay = 5f;
    [SerializeField] private Transform _hook = null;
    [SerializeField] private Transform _marker = null;
    [SerializeField] private Info[] _fish = {};

    private int[] _count;
    private bool  _waitForSpawn = false;

    private HashSet<FishController> spawnedFish = new HashSet<FishController>();

    private void Start() {
        _count = new int[_fish.Length];
        SpawnAll();
    }

    private void Update() {
        if (_waitForSpawn) return;

        bool full = true;
        for (int i = 0; i < _fish.Length; ++i) {
            if (_count[i] < _fish[i].maxCount) {
                full = false;
                break;
            }
        }

        if (!full) {
            // This is really bad lol
            int i = Random.Range(0, _fish.Length);
            while (_count[i] >= _fish[i].maxCount) i = Random.Range(0, _fish.Length);

            Spawn(i);
        }

        StartCoroutine(WaitForSpawn());
    }

    private void Spawn(int index) {
        _count[index] += 1;
        var fish = _fish[index];

        float y = Random.Range(fish.minDepth, fish.maxDepth);
        float x = Random.Range(transform.position.x, _marker.position.x);

        var newFish = Instantiate(fish.prefab, transform);
        newFish.transform.position = new Vector3(x, transform.position.y - y, transform.position.z);

        var controller = newFish.GetComponent<FishController>();
        controller.Hook = _hook;
        controller.RodManager = _rodManager;
        controller.type = index;
        spawnedFish.Add(controller);
    }

    private void SpawnAll() {
        for (int i = 0; i < _fish.Length; ++i) {
            while (_count[i] < _fish[i].maxCount) Spawn(i);
        }
    }

    public void RemoveFish(FishController fish) {
        _count[fish.type] -= 1;
        spawnedFish.Remove(fish);
        Destroy(fish.gameObject);
    }

    public void UpdateHook(Transform hook) {
        this._hook = hook;
        foreach (var fish in spawnedFish) {
            fish.Hook = hook;
        }
    }

    private IEnumerator WaitForSpawn() {
        _waitForSpawn = true;
        yield return new WaitForSeconds(_spawnDelay);
        _waitForSpawn = false;
    }
}
