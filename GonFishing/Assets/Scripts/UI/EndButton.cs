using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Button))]
public class EndButton : MonoBehaviour {
    private void Start() {
        GetComponent<Button>().onClick.AddListener(() => {
            SceneManager.LoadScene("Start");
        });
    }
}
