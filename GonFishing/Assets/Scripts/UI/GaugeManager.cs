using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GaugeManager : MonoBehaviour {
    [SerializeField] private Image _fill = null;
    [SerializeField] private float _decay = 0.2f;

    [SerializeField] private RodManager _rodManager = null;

    private float _currValue = 0f;

    private void Update() {
        _currValue -= _decay * Time.deltaTime;

        if (_currValue >= 1f) {
            _rodManager.Escape();
        }

        _currValue = Mathf.Clamp(_currValue, 0f, 1f);
        _fill.fillAmount = _currValue;
    }

    public void AddValue(float val) {
        _currValue += val;
    }
}
